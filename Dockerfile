FROM bitnami/laravel:8.5.8

WORKDIR /app

COPY . .

RUN composer install

RUN cp .env.example .env

RUN php artisan key:generate && php artisan -q jwt:secret

# RUN php artisan migrate

RUN npm install

CMD ["bash"]